program integral_importance
integer num, num_x, num_i
parameter (num=500000)
real*8 u(num),x(num),y(num)
real*8 sum, factor


do j= 1,5
num_i = 10**j
do i = 1, num   
call random_number(u(i))
call random_number(y(i))
y(i) = -5.0+10.0*y(i)
if (u(i) .le. exp(-y(i)*y(i)/2.0)) then
num_x = num_x +1
x(num_x) = y(i)
end if
if(num_x .eq. num_i) goto 20
end do

20 continue 

! integrant exp(-x^2/2)*(5+cos(x)) 
! exact result:(14.053480445584874, 1.560249756680773e-13)

factor=sqrt(2.0*3.1415926)
sum = 0.0
do i = 1, num_i
!sum = sum + 1.0*x(i)
sum = sum + 1.0*(5.0+cos(x(i)))
end do
write(*,*) num_i, factor*sum/num_i, num_i


end do

end
