program random_exp
parameter (num=100000)
real*8 u(num),x(num)
real*8 lambda
parameter (lambda=1.0/3.0)


open(300, file='random_exp.dat',status='new')
do i = 1, num   
call random_number(u(i))
x(i) = -log(u(i))/lambda
write (300,*) x(i)
end do
close(300)




end 
