program random_uniform
integer num
parameter (num=100000)
real*8 u(num)


open(300, file='random_uniform.dat', status='new')
do i = 1, num   
call random_number(u(i))
u(i) = -5.0 + 10.0*u(i)
write(300,*) u(i)
end do
close(300)

end 
