program integral_unbiased
parameter (num=100000)
integer num_i
real*8 y(num)
real*8 sum, factor


do j= 1,5
num_i = 10**j

do i = 1, num_i   
call random_number(y(i))
y(i) = -5.0+10.0*y(i)
end do

! integrant exp(-x^2/2)*(5+cos(x)) 
! exact result:(14.053480445584874, 1.560249756680773e-13)

factor = 10.0
sum = 0.0
do i = 1, num_i
sum = sum + exp(-y(i)*y(i)/2.0)*(5.0+cos(y(i)))
end do
write(*,*) num_i, factor*sum/num_i, num_i


end do

end
